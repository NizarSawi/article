import React from 'react'
import './App.css';
import Addarticle from './Pages/Addarticle';
import Header from './Component/Header';
import Footer from './Component/Footer';
import Home from './Pages/Home';
import View from './Pages/View';
import {BrowserRouter as Router,Route,Routes} from 'react-router-dom'
import Editarticle from './Pages/Editarticle';
import Addscreen from './Pages/Addscreen';

function App() {
  return (
    <Router>
    <div className="App">
      <Header/>
     <Home/>
    
     <Routes>
     
     <Route  exaxt path = "/addarticle" element={<Addarticle />}/>
     <Route  exaxt path = "/editarticle" element={<Editarticle />}/>
     <Route  path = "/view" element={<View />}/>
     <Route  path = "/addscreen/:rowid" element={<Addscreen/>}/>
     
     
   </Routes> 
      </div>
    </Router>
    
  );
}

export default App;
