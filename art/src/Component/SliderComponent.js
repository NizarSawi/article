import React, { useState } from "react";
import '../Style/SliderComponent.css'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const SliderComponent=(props)=> {
  const [data,setData]=useState([props])
  
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };
  return (
    <div>
    {data.map((d,index)=>{
      return(
       <div>
           <Slider {...settings}>
      <div>
        
      <card>{d.title}</card>
      </div>
      <div>
        <h3>{d.body}</h3>
        {console.log(data.title)}
      </div>
    
    </Slider>
       </div> 
      )
    })}
  
  
  </div>
  )
}
export default SliderComponent
