import React from 'react';
import {Container,Navbar,Nav,Image } from 'react-bootstrap';
import logo from '../Images/logo.png'
import "bootstrap/dist/css/bootstrap.css";
import { Link } from 'react-router-dom';


const Header = () => {
  
  return <div>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
            <Navbar.Brand   href="#home">Articles Managament System</Navbar.Brand>
            <Link to={"/view"}><Navbar >View</Navbar></Link>
            
            <Nav>
                <Nav.Link eventKey={2} href="#logo">
                <Image src={logo} responsive />
                </Nav.Link>
                </Nav>
            </Container>
        </Navbar>
  </div>;
};

export default Header;
