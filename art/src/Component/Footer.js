import React from 'react';
import {Card} from 'react-bootstrap';
const Footer = () => {
  return <div>
      <Card collapseOnSelect expand="lg" bg="dark" variant="dark">
       <Card.Body>@2022</Card.Body>
      </Card>
  </div>;
};

export default Footer;
