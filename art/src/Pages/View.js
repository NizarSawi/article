import React, { useState,useEffect } from 'react';
import { Form, Button,ButtonGroup, Card } from "react-bootstrap";
import '../Style/View.css'
import moment from 'moment'
import SliderComponent from '../Component/SliderComponent';
import {Link,useParams} from 'react-router-dom'


const View = () => {
  const {rowid}=useParams();
  const [items,setItems]=useState([''])
  const [screen,setScreens]=useState([])
 console.log(rowid)
    useEffect(()=>{
      const data = localStorage.getItem('data')
      if(data){
        setItems(JSON.parse(data))
      }},[])

      useEffect(()=>{
        const data = localStorage.getItem('screens')
        if(data){
          setScreens(JSON.parse(data))
        }},[])


  return (
    <>
  <SliderComponent data={screen} />
  <div className='main'>
    
     {items.map((i,index)=>{
         return(
         <div>
           
          {items.id==screen.id ?
          <Card key={index} style={{ width: '12rem',borderRadius:'0.5rem'}}>
                       <Card.Img style={{ borderRadius:'0.5rem'}} variant="top" src={i.upload} />
          <Card.Body>
              <Card.Title>{i.category}</Card.Title>
              <Card.Text>
               {i.name}
              </Card.Text>
              <Button variant="primary">{moment(i.rtime, "LT").fromNow()}</Button>
              
          </Card.Body>
            </Card>
       :null}
            </div>
          
                  )
              })}
                </div>
            </>
            )
          };

export default View;
