import React, { useState,useEffect } from 'react';
import '../Style/Home.css'
import {Link} from 'react-router-dom'
//import "bootstrap/dist/css/bootstrap.css";
//import "react-bootstrap-table-next/dist/n.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import {Container,Button } from 'react-bootstrap';
import { FcDeleteColumn } from "react-icons/fc";
import { FcEditImage } from "react-icons/fc";
import { FaRegAddressBook } from "react-icons/fa";
import Addarticle from './Addarticle';
const columns = [{
    dataField: 'id',
    text: 'Article ID',
    sort: true
  }, {
    dataField: 'name',
    text: 'Article Name',
    sort: true
  }, {
    dataField: 'category',
    text: 'Article Price'
  },
  {
    dataField: 'rtime',
    text: 'Reading Time',
    sort: true
  },
  {
    dataField: 'action',
    text: 'Actions',
    formatter: (cell, row, rowIndex, formatExtraData) => { 
     const rowid=rowIndex
      
      return (  
        
        <> 
       <Link to={`/editarticle`}><span className='policon'><FcEditImage /></span></Link>
       <Link to={"deletearticle"}><span className='policon'><FcDeleteColumn /></span></Link>
       <Link to={`addscreen/${rowid}`}><span className='policon'><FaRegAddressBook /></span></Link>
       
        </> 
        
      )
    }
}
];
 
  
  const defaultSorted = [{
    dataField: 'name',
    order: 'desc'
  }];
const Home = () => {
    const [showrows,setShowrows]=useState(false)
    const[data,setData]=useState([])

    const handleaddarticle=()=>{
        setShowrows(true)
    }
        useEffect(()=>{
            const data = localStorage.getItem('data')
            if(data){
              setData(JSON.parse(data))
              console.log(data)
             }
           
            },[])
    
  return <div>
      <Button  onClick={handleaddarticle} className='add-btn'>Add Article</Button>
  
      <br/>
      {showrows ? <Addarticle /> : null}
      <BootstrapTable keyField='id' data={ data } columns={ columns }  defaultSorted={ defaultSorted }  />
  </div>;
};

export default Home;
