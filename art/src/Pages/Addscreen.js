import React,{useState,useEffect} from 'react';
import {Link,useParams} from 'react-router-dom'
import Card from 'react-bootstrap/Card'

const Addscreen = () => {
  const {rowid}=useParams();
  const [title, setTitle] = useState('')
  const [body,setBody]=useState('')
  
  const [item2,setitem2]=useState([]) 

const handleSubmit = (e) => {
    e.preventDefault();
    setitem2([...item2,{id:rowid,title:title,body:body}])
    setTitle('')
    setBody('')
   
  };
  useEffect(()=>{

    const data2 = localStorage.getItem('screens')
    
    if(data2){
      setitem2(JSON.parse(data2))
     }
    console.log(item2)
    },[])
  
    useEffect(()=>{
  
      localStorage.setItem('screens',JSON.stringify(item2))
    
    })

  
  return <div>
    <form onSubmit={handleSubmit} style={{ width: '18rem' }}>
    <div class="col">
       <input  type="text" class="form-control" placeholder={"Title"} value={title} onChange={(e) => setTitle(e.target.value)} />
   </div>
   <div class="col">
       <input  type="text" class="form-control" placeholder={"Body"} value={body} onChange={(e) => setBody(e.target.value)} />
   </div>
   <div class="col">
       <input  type="submit" class="form-control" value={"ADD"} />
   </div>
</form>
  </div>;
};

export default Addscreen;
