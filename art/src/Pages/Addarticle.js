import React, { useState,useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Container } from 'react-bootstrap';
import {Link,useParams} from 'react-router-dom'
const Addarticle = () => {
  const {rowid}=useParams();
  const [name, setname] = useState('')
  const [category,setCategory]=useState('')
  const [rtime,setRtime]=useState('') 
  const [id,setid]=useState(0)
  const [upload,setupload]=useState('')
  const [item,setitem]=useState([]) 

const handleSubmit = (e) => {
  setid(id+1)
  console.log(id)
    e.preventDefault();
    setitem([...item,{id:id,name:name,category:category,rtime:rtime,upload:upload}])
    setid(id+1)
    setname('')
    setCategory('')
    setRtime('')
    setupload('')
  };
 

useEffect(()=>{
  const data = localStorage.getItem('data')
  if(data){
    setitem(JSON.parse(data))
   }
  console.log(item)
  },[])

  useEffect(()=>{
    localStorage.setItem('data',JSON.stringify(item))
  })

  return <div>
        <Container container-md	>
         <form onSubmit={handleSubmit}>
          <div class="row">
          <div class="col">
              <input readOnly={true} type="number" class="form-control" placeholder={id} value={id} onChange={(e) => setid(e.target.value)}/>
            </div>
            <div class="col">
              <input type="text" class="form-control" placeholder=" name" value={name} onChange={(e) => setname(e.target.value)}/>
            </div>
            <div class="col">
              <input type="text" class="form-control" placeholder=" Category" value={category} onChange={(e) => setCategory(e.target.value)}/>
            </div>
            <div class="col">
              <input type="text" class="form-control" placeholder=" img address link" id="upload" value={upload} onChange={(e) => setupload(e.target.value)}/>
            </div>
            <div class="col">
              <input type="time" class="form-control" placeholder=" Reading time" value={rtime} onChange={(e) => setRtime(e.target.value)}/>
            </div>
            <div class="col">
            <input type = "submit"/>
           
            </div>
          </div>
       
        </form>
          </Container>    
  </div>;
};

export default Addarticle;
